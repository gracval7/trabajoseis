package trabajoPresentar;

import net.java.html.boot.BrowserBuilder;

class DesktopMain {

    public static void main(String[] args) {
        BrowserBuilder.newBrowser().loadPage("pages/index.html")
                .loadFinished(Demo::onPageLoad)
                .showAndWait();
        Ventana v1 = new Ventana();
        v1.setVisible(true);
        System.exit(0);
    }
}
