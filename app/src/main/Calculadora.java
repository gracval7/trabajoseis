
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author hp
 */
public class Calculadora extends JFrame{
    private JTextField numero1;
    private JTextField numero2;
    private JComboBox<String> operacion;
    private JButton calcular;
    private JLabel resultado;


    public Calculadora(){

        setSize(500,500);

        setTitle("Calculadora");

        setSize(400, 200);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);
        
        JLabel numero1Label = new JLabel("Ingrese valor 1:");
        numero1Label.setBounds(50, 10, 100, 30);
        add(numero1Label);

        numero1 = new JTextField();
        numero1.setBounds(50, 30, 100, 30);
        add(numero1);


        JLabel numero2Label = new JLabel("Ingrese valor 2:");
        numero2Label.setBounds(200, 10, 100, 30);
        add(numero2Label);
        numero2 = new JTextField();
        numero2.setBounds(200, 30, 100, 30);
        add(numero2);

        String[] operaciones = {"+", "-", "*", "/"};
        operacion = new JComboBox<>(operaciones);
        operacion.setBounds(150, 30, 50, 30);
        add(operacion);

        calcular = new JButton("Calcular");
        calcular.setBounds(150, 70, 100, 30);
        add(calcular);

        resultado = new JLabel("Resultado: ");
        resultado.setBounds(50, 110, 300, 30);
        add(resultado);

        calcular.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double num1 = Double.parseDouble(numero1.getText());
                    double num2 = Double.parseDouble(numero2.getText());
                    String op = (String) operacion.getSelectedItem();
                    double res = 0;

                    switch (op) {
                        case "+":
                            res = num1 + num2;
                            break;
                        case "-":
                            res = num1 - num2;
                            break;
                        case "*":
                            res = num1 * num2;
                            break;
                        case "/":
                            if (num2 != 0) {
                                res = num1 / num2;
                            } else {
                                JOptionPane.showMessageDialog(null, "División por cero no permitida.");
                                return;
                            }
                            break;
                    }
                    resultado.setText("Resultado: " + res);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Por favor ingrese números válidos.");
                }
            }
        });
    }

}
